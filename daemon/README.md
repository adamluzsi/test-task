Daemon
===========

Your task is to create a daemon/service application.

Not ALL the following features required to make you pass the Test. 
This test is all about measure your programming and architect abilities with an example project.

Design a Daemon that has the following specifications:
 * It should be able to manage to managed through CLI
 * It should use the bare minimum resource to stay alive
 * it should be able to start and listen in at least one of the following channel with non blocking terminology
 ** Linux based named pipe (FIFO) 
 ** Linux TCP Socket based worker
 ** RabbitMQ consumer for a given queue_name

 * It should be able to write down received data to a a csv file that is tab separated, and contains the processed at timestamp
 * It should be able to receive CLI options for configuring purpose
 ** such as:
 *** socket port/file
 *** Pid File location
 *** log file location
 *** Anything that you value enough, to be useful for your applications end-user 

Major Tasks:
 * You should use ruby
 * You should use git as vcs
 * You shouldn't write pseudo code
 * You should write code that you would commit/push to working, well used repository.
 * You should submit even if you are not fully finished.

it is not mandatory, but we only just get glad if:
 * You use BDD/TDD approach for development
 * You use clean code
 * You make the application self documenting for end user
 * Try use the bare minimum external modules/gems 

If you have any further question, don't hesitate to ask.
