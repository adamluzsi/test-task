Using Ruby, generate an N x N grid, where N can be any number, and randomly populate the grid with letters (A-Z).

Using the provided dictionary file find all by the following lookup logics:

Horizontal words from left to right in your grid
Horizontal words from right to left in your grid
Vertical words from top to bottom in your grid
Vertical words from bottom to top in your grid
Diagonal words from left to right in your grid
Diagonal words from right to left in your grid
If possible, provide unit tests.

Major Tasks:
 * You should use ruby
 * You should use git as vcs
 * You shouldn't write pseudo code
 * You should write code that you would commit/push to working, well used repository.
 * You should submit even if you are not fully finished.

it is not mandatory, but we only just get glad if:
 * You use BDD/TDD approach for development
 * You use clean code
 * You make the application self documenting for end user
 * Try use the bare minimum external modules/gems

If you have any further question, don't hesitate to ask..
