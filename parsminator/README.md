Parsminator
===========

Your task is to create a module that implements a custom serialization for a message format, that has following dependencies 

the message object should be able to
* tell the sender name
* the message body/text
* the message and the sender name both should be able to contain any kind of letter with no exeption

The Parser object
* should be able to dump (serialize) the message object to it's own format
* should be able to unserialize the serialized message into an Message object

* You shouldn't use any library 
* You shouldn't write pseudo code. You should write code that you would commit/push to a repository and solves the given problem. 
* You have 120 minutes to submit your solution in the FORKED repository
* You should submit even if you are not fully finished. 

If you have any further question, don't hesitate to ask.
